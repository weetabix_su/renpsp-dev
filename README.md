## RenPSP Development Repository

RenPSP is a Lua Player clone of the Ren'Py Visual Novel Engine for the PlayStation Portable platform.
This repository will serve as a developmental testbed for edited as well as new features of the RenPSP 0.3 release onwards. It will also contain select developmental visual novels, if possible.

RenPSP is free, but there are some rules of it's rebistribution:
* inform lolbot_iichan@mail.ru or kraftwerk.renpsp@gmail.com about your actions
* provide link to http://iichan.hk or http://iichan-eroge.blogspot.com with your release
* do not try to earn money using RenPSP or it's parts

Note: LuaPlayer and Lua Player Plus is licensed under GNU General Public License; Lua Player Euphoria is licensed under OSI BSD 3-Clause License (a.k.a. New BSD License)

### Start on Microsoft Windows:
    just run: win32_start.bat
    tested at Windows XP, Windows 7, and Windows 8 
    MS Windows release is based on LuaPlayer 0.20
    LuaPlayer 0.20 exists on PSP 1.50 too
    
### Start on cracked PSP as homebrew:
    put everything in ms0:/PSP/GAME/
    Lua Player Euphoria v8 is the default Lua Player EBOOT.PBP
    rename EBOOT_LPP.PBP to EBOOT.PBP to use Lua Player Plus
    tested at PSP FAT with 5.00 M33-3 and 6.60 ME

## FEATURES:
+ Support for .rpy scripts, image formats (JPEG, PNG, BMP), audio formats (WAV, MP3, OGG Vorbis, Atrac3)
+ Single save file function
+ Cyrillic 1251 character map compatibility

## CHANGELOG:

### 0.4
+ New default skin
+ Set Lua Player Euphoria v8 as default Lua Player
+ Fixed image memory leaks from sprites and backgrounds (improved as of 0.4a)
+ Game skin and helpfile customization
+ Custom game skin variables corrected (as of 0.4a)

### 0.3
+ Added LuaPlayer Plus r163 EBOOT with the release
+ Images loaded when used, not upon initialization
+ Full paths used for most files
+ Use of RenPSP logfile
+ 8 New Character Positions

### 0.2
+ Skip function stops correctly
+ Settings, Help File, Test script, and Dropdown menu changed from Russian to English
+ Fixed If-Else Test in Test script
+ UI Changes (Black dialog and answer boxes, Katawa Shoujo SD Character Icons for the dropdown menu)

### 0.1
+ Initial Release

(All further changes in this repository may not be reflected in this changelog)

## DISCLAIMER:
RenPSP is an independent release of lolbot from the iichan Eroge Team. RenPSP is NOT affiliated with iichan Eroge unless further specified.
This development repository is managed by Mikko Luis Saavedra (weetabix) who can be contacted via e-mail [vovo27_miranemiko@yahoo.co.jp] or Twitter [@weetabix_su].
Content used in RenPSP games may be protected by any form of licensing and may be publishing a derivative of the protected game. It is your responsibility to use the content at your own risk.
