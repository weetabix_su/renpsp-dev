init:
    image weet cred = "img/portcred.jpg"
    image weet title = "img/main_menu.png"
	
	image bg city_evening = "img/city_evening.jpg"

    image uncle_mugen smile = "img/uncle_mugen_smile.png"
    image uncle_mugen smile2 = "img/uncle_mugen_smile_2.png"

    image a tell = "img/tell.png"

    image a 01 = "img/a_01.png"
    image a 02 = "img/a_02.png"
    image a 03 = "img/a_03.png"
    image a 04 = "img/a_04.png"
    image a 05 = "img/a_05.png"

    image slide 01 = "img/slide_01.png"
    image slide 02 = "img/slide_02.png"
    image slide 03 = "img/slide_03.png"
    image slide 04 = "img/slide_04.png"
    image slide 05 = "img/slide_05.png"
    image slide 06 = "img/slide_06.png"
    image slide 07 = "img/slide_07.png"
    image slide 08 = "img/slide_08.png"
    image slide 09 = "img/slide_09.png"
    image slide 10 = "img/slide_10.png"
    image slide 11 = "img/slide_11.png"
    image slide 12 = "img/slide_12.png"
    image slide 13 = "img/slide_13.png"
    image slide 14 = "img/slide_14.png"
    image slide 15 = "img/slide_15.png"
    image slide 16 = "img/slide_16.png"
    image slide 17 = "img/slide_17.png"
    image slide 18 = "img/slide_18.png"
    image slide 19 = "img/slide_19.png"
    image slide 20 = "img/slide_20.png"
    image slide 21 = "img/slide_21.png"
    image slide 22 = "img/slide_22.png"
    image slide 23 = "img/slide_23.png"
    image slide 24 = "img/slide_24.png"
    image slide 25 = "img/slide_25.png"
    image slide 26 = "img/slide_26.png"
    image slide 27 = "img/slide_27.png"
    image slide 28 = "img/slide_28.png"
    image slide 29 = "img/slide_29.png"
    image slide 30 = "img/slide_30.png"
    image slide 31 = "img/slide_31.png"
    image slide 32 = "img/slide_32.png"
    image slide 33 = "img/slide_33.png"
    image slide 34 = "img/slide_34.png"
    image slide 35 = "img/slide_35.png"
    image slide 36 = "img/slide_36.png"
    image slide 37 = "img/slide_37.png"
    image slide 38 = "img/slide_38.png"
    image slide 39 = "img/slide_39.png"
    image slide 40 = "img/slide_40.png"
    image slide 41 = "img/slide_41.png"
    image slide 42 = "img/slide_42.png"
    image slide 43 = "img/slide_43.png"

label start:
	scene weet cred
	$renpy.pause(2)
    play music "sunshine_a.ogg"
    scene weet title 
    $renpy.pause(4)
label init:
    scene bg city_evening
    show uncle_mugen smile at left
    "Good day my friend..."
    "I see you come here to seek the truth..."
    "You have come to the right place..."
    "Allow me to introduce myself..."
    "I am Uncle Mugen..."
    "Please stay a while as you and me... well mostly me..."
    "Tell you a few things you need to learn to understand Marijuana."
    "And do not worry... I will try and explain it in friendly easy to grasp words..."
    "Though we might use a few uncommon terms here and there... but what the heck..."
    "I'll try explain them as much as I can..."
    "Let's get started..."
    show slide 01 at right
    "When you were young... you probably heard from your Parents, Grandparents, Teachers and others..."
    "That Marijuana is bad for your health..."
    "And understandably, you'd believe them and grow up with the knowledge firmly embedded into you..."
    "That Marijuana is bad for your health and should avoid it."
    show slide 02 at right
    "And in some cases, Organizations like D.A.R.E (Drug Abuse Resistance Education) has come to your school..."
    "Or perhaps in some posters or phamplet..."
    "Lecturing about the bad effects of Marijuana in greater detail..."
    show slide 03 at right
    "Not only that, you probably got told that you could get in trouble with the law when you associate yourself with Marijuana..."
    "I mean... if it's in the newspaper or being told by someone famous or influential then it must be right..."
    show slide 04 at right
    "Is it?..."
    "Have you ever wondered who gets to decide what's right and what's wrong?"
    "Or perhaps wondered if the decision those decision makers made is for the best interest?"
    "Unfortunately... Marijuana isn't one of them..."
    "Now... now... you are probably thinking that I'm just a lowlife scum enticing you to try Marijuana..."
    "If you think I am then to prove to you that I have no ulterior motives..."
    "You can always close this program and delete it from your computer."
    "I won't force you to listen or believe me..."
    "Now where were we?... Ah yes..."
    show slide 05 at right
    "If you asked the average person about Marijuana... they'll probably say that..."
    show slide 06 at right
    "Marijuana is Bad for your Health..."
    show slide 07 at right
    "Marijuana is Addictive..."
    show slide 08 at right
    "It can make you Crazy and Insane..."
    show slide 09 at right
    "It can kill you..."
    "...and so forth"
    "What if I tell you that everything you heard or learned from your parents, school, preachers about Marijuana..."
    show slide 10 at right
    "Is a lie..."
    "What if I tell you that Marijuana is far more safer than the usual legal substances people abuse regularly..."
    "What if I tell you that Marijuana is made illegal not because it's bad for your health..."
    "But to protect the interests of the powerful few..."
    "Would you believe me?"
    "Well?..."
    show slide 11 at right
    "Oh OK fine... let's go to the basics..."
    "What is Marijuana?..."
    "Well... Marijuana A.K.A. Cannabis... is a plant, a naturally growing plant found in nature."
    "Actually... I think it's much better to use the term Cannabis than Marijuana..."
    "Yes... I think from this point I'll refer to Marijuana as Cannabis."
    "Since the term Marijuana is deregatory and used by DuPont for their false propaganda..."
    "I'll tell more about that later... but for now let's focus on the plant itself..."
    show slide 12 at right
    "Cannabis is widely used throughout history..."
    "Accourding to wikipedia... the one stop website for copy-pasta homework..."
    "Cannabis has been used as far back as 3rd millennium BC..."
    "Let's see... 3rd Millenium BC is about 3000 years before Jesus was born plus another 2000 plus..."
    "That means Cannabis has been used by humans for more than 5000 years..."
    "Probably even longer... who knows..."
    show slide 13 at right
    "Cannabis has so many uses..."
    "It's been used for Religious Ceremonies, Medicinal, Food, Ropes and even Cloth."
    "Ironically, the earliest known sample of fabric is made from... Hemp."
    "And Hemp is Cannabis."
    "Lol... there was even a time when Marijuana... ehem... excuse me..."
    "I mean Cannabis can be used as legal tender..."
    "That's how important and how deeply embedded Cannabis is in human culture."
    show slide 14 at right
    "And then we go to the next question and ask?"
    "If Cannabis is this widely used back then... why is it illegal now?"
    "Let's see... it's actually slightly more complicated but I'll try and make it simple..."
    "There are actually several things that happened late in the 19th century and early 20th century..."
    show slide 15 at right
    "That gave Cannabis a bad name like how the (now discredited) Egyptian Delegation from the 1924 Opiates Conference in Geneva..."
    "Whose Cotton exports are threathened by Hemp (A.k.a. Cannabis)..."
    "Claimed that cannabis was dangerous and addictive."
    "That it made some people go insane after smoking Cannabis... which we now all know is big fat lie..."
    "But probably one of the single biggest motivation for going so far and demonize Cannabis is is..."
    show slide 16 at right
    "...Greed... Human Greed"
    "Now where should we start?"
    show slide 17 at right
    "According to my awesome Google-Fu..."
    "During the Mexican Revolution of 1910, Poncho Villa's occupying forces were holding on a number of forests."
    show slide 18 at right
    "Owned (No he did not) and logged by the Hearst Publishing Co. CEO, William Hearst."
    "Hearst, being an influential and powerful person during those times started a smear campaign shorly after against the influx of Mexican immigrants."
    "Unfortunately, Cannabis usage was widespread among the Mexicans and the reputation of Cannabis also got smeared..."
    show slide 19 at right
    "For this they used the Racist Mexican term... Marijuana..."
    show slide 20 at right
    "Of course people being so naieve back then believed every propaganda they saw on the news."
    "I mean... it was in the news then it must be true... right?"
    show slide 21 at right
    "And there are also these large pharmaceutical companies who are patenting new drugs."
    "Which see Cannabis as a threath since they cannot patent a naturally growing substance."
    "And since you cannot patent Cannabis..."
    "You cannot make money out of them..."
    show slide 22 at right
    "Same thing with Petrochemical companies like (FUCK YOU!) DuPont..."
    "They were preparing to monopolise fuel supplies and replace natural Cannabis derived products by synthetics..."
    "Such as plastics and nylon, and take control of the paper industry, and supply vast amounts of chemical fertilisers and pesticides..."
    "For less sturdy crops such as cotton and tobacco."
    show slide 23 at right
    "And by the 1930 lobbying efforts are starting to ramp up."
    "And thanks to the efforts of fine bastards like William Hearst."
    "Companies such as DuPont, and very influential public servants such as Anslinger."
    "Who is most likely supported by assholes from Dupont... (actually, he was...)"
    show slide 24 at right
    "Cannabis was almost completely illegal by 1936."
    "And they did it at lightning speed that the American Medical Association which recognised Cannabis as a medicine."
    show slide 25 at right
    "Wasn't even aware that Marijuana that is being made illegal was actually in fact Cannabis."
    "And by the time they realized it was too late... despite the backing of scientific papers..."
    "Yellow journalism prevailed... I mean William Hearst happen to be one of the largest media moguls of his time..."
    "And spreading propaganda favoring his cause is as simple as having it printed on his newspapers..."
    "Remember... back then Newspaper = Truth as far as the ordinary people is concerned despite being a big fat lie..."
    "Of course we now all know the horrible truth and the circumstances as to why."
    hide slide
    "And now we come to another important question..."
    "Now that we know Marijuana is unfairly labeled and is actually a victim of human greed..."
    "To protect the selfish interests of the powerful few..."
    show slide 14 at right
    "Why is it mostly still illegal?"
    "Well... you probably are already aware the power of the media as a propaganda tool."
    show slide 26 at right
    "During the early part of the 20th century, the Marijuana prohibitionists."
    show slide 27 at right
    "Wanted to protect their own interests started a prolonged and thourough smear campaign."
    show slide 28 at right
    "Spreading lies and conditioning the public that Marijuana is addictive and is bad for you."
    show slide 29 at right
    "They even go as far as make a movie called REEFER MADNESS..."
    "That highlights the so called DANGERS of using Cannabis."
    hide slide
    "The same propaganda machine also conditioned the naive masses back then..."
    "Spreading lies that Cannabis had no medical use."
    show slide 30 at right
    "Another thing that really hurt the reputation of Marijuana is it's association with uh... questionable lifestyles..."
    "Such as Hippies, NEET's and Losers..."
    show slide 31 at right
    "There is also this thing where people who grew up believing the false lies about Marijuana as truth."
    "Are somewhat uncomfortable to accept the truth..."
    show slide 32 at right
    "Especially people like politicians who have so much to lose"
    "Even though they are fully aware of this issue."
    hide slide
    "I mean it's been banned for a long time why change it now?"
    show slide 33 at right
    "You see... that is a problem..."
    "You have a plant with so much potential to change the world in positive ways."
    "We know Marijuana is a Food, We know Marijuana is a Medicinal Herb... "
    "We know Marijuana may revolutionize the industry of fabric..."
    "We know that Marijuana is totally safe to your health..."
    "And the only thing that prevents us from accessing and further develop the potential of this marvelous plant..."
    "Are the policies put in place by Government Officials who are most likely in cahoots with large companies like DUPONT and others..."
    "Who wanted to protect their own selfish interests..."
    "I mean... have you tried Marijuana?..."
    "I have... and I can 100 Percent say that all the rubbish about Marijuana being addictive, will make you mad are total lies..."
    show slide 34 at right
    "In fact... I dare say the common Tobacco sticks are a far greater health hazzard and should be outright banned!"
    "In the US alone there are 443,000 deaths annually (including deaths from secondhand smoke)"
    "And that is for 2011 alone..."
    "Deaths that can be directly attributed to Tobacco..."
    "While there have been no deaths EVER for Marijuana usage."
    "Even among those who use them daily and is backed by proper scientific reaserch."
    show slide 35 at right
    "And let's not even get started on the so called macho War on Drugs..."
    show slide 36 at right
    "We already know what happened with Alcohol prohibition..."
    "You ban alcohol violent gangs take over and so forth."
    "You lift the ban... gangs go away..."
    show slide 37 at right
    "Same thing today... you made Marijuana illegal... criminals step in."
    "And fill in demand..."
    show slide 38 at right
    "Unfortunately, disputes between rival gangs can only be resolved via violence."
    "Which ends up having civilians getting caught between."
    "And as long as world governments hang stubornly to this prohibition..."
    "Don't expect violent gangs to dissapear..."
    show slide 39 at right
    "Heck! Knowing how lucrative the drug trade made possible by stupid Government policies..."
    "I think I myself might even join in the fun and get rich from a harmless medicinal herb."
    "I mean they made it illegal to protect their interests... why not capitalize on this Golden Oportunity... "
    "Grow them clandestinely and control the distribution and the profits!"
    "You know, make myself rich and powerful out of Governments stubborn policies... Hmm?"
    hide slide
    "Ehem... let's save that for later OK... Please pretend you didn't hear that... Lol!..."
    "But you know what really made the War on Drugs a total failure?"
    show slide 40 at right
    "Is the fact that they lumped Marijuana with the genuinely dangerous stuff."
    "Of course people know that Marijuana not only doesn't belong with these group of dangerous substances."
    "But is also one of the safest, harmless substance known to mankind."
    "And by lumping Marijuana in the mix... the whole War on Drugs now becomes a joke."
    "A joke where no one takes it seriously now and is nothing more than a waste of time and resources."
    "A really funny one but painfully expensive for those who are touched by it."
    show slide 41 at right
    "Anyway... we already have a ton of anecdotes and testimonies backed by scientific studies on how Marijuana have relieved them."
    "From various illnesses that no legal drug has done..."
    show slide 42 at right
    "Of course you can expect the Pharmaceutical industry to make a move and counter this by publishing more lies..."
    "In form of scientific studies highlighting non-existing harmful effects of Marijuana."
    "To protect their interests and market from being infiltrated by Marijuana."
    show slide 43 at right
    "Which deny legitimate users who badly needed it..."
    "Leaving them no choice but to deal with dangerous criminals who has them at ridiculously high prices..."
    "Don't you think it's silly to pay top dollar for a naturally growing medicinal herb?"
    hide slide
    "Oh OK... not everyone in this world is dumb enough to fall for the prohobitionists..."
    "In some states in the US... Marijuana is legal to use for medical purposes..."
    "While it's still subject to stringent proceedures and somewhat difficult process of acquiring Marijuana..."
    "It's still a step towards the right direction."
    "And while the lies of Dupont, Hearst and Anslinger are already exposed to the world... we still have much to do to reverse it's effects."
    "We should start by teaching children in schools and in our homes about what really Marijuana is."
    "Shed away it's association with hippies and instead promote it's legitimate and responsible use."
    "And expose the lies of those who spread false information demonizing Marijuana to protect their interests."
    show uncle_mugen smile2
    "I'm already old and will most likely no longer be here when the day comes that Marijuana is finally seen as it is"
    "As a Medicinal Herb with Legitimate uses and is readily available to those who will need it."
    "This is my small my contribution to the cause and to correct the mistakes of the past."
    "And hopefully, raise awareness and not repeat the same stupid mistakes again."
    "Please... Tell your families and friends..."
    "Help the world spread the truth about Marijuana, it's tale and the people responsible for it's demonization."
    "Do it for yourself, for the future generations of children who will one day have easy access to this wonderful plant."
    scene white
    $ renpy.pause(2)
    scene a tell
	$ renpy.pause(5)
    scene white
    $ renpy.pause(3)
    scene a 01
    $ renpy.pause(1)
    scene a 02
    $ renpy.pause(3)
    scene a 03
    $ renpy.pause(3)
    scene a 04
    $ renpy.pause(3)
    scene a 05
    $ renpy.pause(3)
    scene a 01
    $ renpy.pause(1)
    menu:
		"Replay":
			jump init
		"Exit":
			$renpy.quit()