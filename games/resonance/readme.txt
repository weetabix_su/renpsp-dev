﻿−−−−−−−−−−−−−−−−−−−−−−−−−
　　　　　　　　・　・　◇　・　・

　　　　　　　 A short visual novel


           "A Midsummer Day's Resonance"
           
            	    RenPSP Port

       Written and directed by Kagura Saitoh

　　　　　　　　・　・　◇　・　・
−−−−−−−−−−−−−−−−−−−−−−−−−

【   Title    】  A Midsummer Day's Resonance
【  Version#  】  1.01E
【   Writer   】  Kagura Saitoh
【 Translator 】  Seung Park

−−−−−−−−−−−−−−−−−−−−−−−−−

【 Copyrights and redistribution 】

This work is copyright (c) 2002-2005 by Kagura Saitoh.
In addition, the sound effects and music used in this work are copyrighted by their respective owners.
You are given unlimited permission to redistribute this work so long as you do not modify its contents and so long as you do not seek to sell it.
However, if you are looking to redistribute this work for profit, please contact Kagura Saitoh at webmaster-at-re-sonance-dot-com.


Music ／
	KeNji
		 HP：DreamN-Hit ～やすらぎの音楽～
		URL：http://www.lunanet.gr.jp/tango/

	ASOBEAT
		 HP：ASOBEAT
		URL：http://yonao.com/asobeat/

Sound Effects ／
	WEB WAVE LIB
		 HP:WEB WAVE LIB
		URL:http://www.s-t-t.com/wwl/

NScripter ／
	Naoki Takahashi
		 HP:Takahashi's Web
		URL:http://www2.osk.3web.ne.jp/~naokikun/

ONScripter ／
	Ogapee
		 HP:Studio OGA
		URL:http://ogapee.at.infoseek.co.jp/

RenPSP ／
	lolbot
		 HP: ???
		URL: http://rghost.net/2000476

−−−−−−−−−−−−−−−−−−−−−−−−−


【 Acknowledgments 】

This software could never have been completed if it were just me alone.

To KeNji and ASOBEAT, who provided such beautiful music,
To WEB WAVE LIB, who provided so many crisp sound effects,
And to Naoki Takahashi, who developed NScripter,
I thank you from the bottom of my heart.
If it weren't for you I'd never have been able to do this.

I'd also like to thank everyone who has been nurturing my clumsy efforts:
To everyone over at Rispetto (http://rispetto.net), especially Susumu Fuji,
As well as all of you who have downloaded and enjoyed this piece,
My profound gratitude.


           4 July 2002 - Kagura Saitoh
      Translated 1 October 2005 - Seung Park

	   Ported 29 May 2012 - weetabix
		 RenPSP by lolbot

−−−−−−−−−−−−−−−−−−−−−−−−−

【 Installation Instructions 】

Simply extract the entire folder of [resonance] to

	ms0:\PSP\GAME\RenPSP\games

(where ms0 is the PlayStation Portable's storage)