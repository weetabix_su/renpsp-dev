﻿
=========================================================================

　　　OMGWTFOTL RenPSP Port version 1.0E readme (2012/05/31)

=========================================================================

◆INTRODUCTION

Thank you very much for downloading "OMGWTFOTL" by HANPAMANIA-SOFT.  This
document includes a brief circle introduction as well as short play
instructions as well as errata and translators' notes, so please read it
carefully before beginning play.


◆ABOUT HANPAMANIA-SOFT

HANPAMANIA-SOFT was founded in 2002 in the wake of the release and critical
acclaim of the doujin visual novel "Tsukihime", under the pipe dream that
all such doujin visual novels could actually do such wonderous things like,
oh, turn a profit; we have since evolved into a circle that produces
original doujin visual novels.  That may or may not turn a profit.

Now, there does exist a jinx that states that "no long original doujin
visual novel will ever be completed", but even so:
- in 2003 we released our first work, "Fuyu ha Maboroshi no Kagami",
- in 2005 we released our second work, "Sora no Ue no Omocha",
- and in 2006 we released our third work, "Comic Days H",
at Comic Market and in doujin stores everywhere.

If, in playing this game, you become interested in playing any of our other
games, we would be very happy if you went to one of the fine shops that carry
our wares (like Toranoana and Melonbooks) and bought one of our other
productions.

We also have Trial Editions of all of our productions up on our website, so
if you would like to give them a try we would be most grateful as well.

※CAUTION
・Our games are generally burdened with a triple curse: they are dirty, very,
  well, "manly", and are quite assertive.  It seems that players are split
  into two camps: those who love our games and those who hate them.  There is
  no middle ground.  Please be sure to play our Trial Editions to make sure
  you'll like our products before buying them.

・Also, "Fuyu ha Maboroshi no Kagami", "Sora no Ue no Omocha", and "Comic
  Days H" are all 18+, and include situations and graphics inappropriate for
  minors.  If you are under the standard age of consent of your nation, please
  refrain from playing our products.

※HANPAMANIA-SOFT OFFICIAL WEBSITE
・http://hms.muw.jp/


◆INSTALLATION

Simply extract the entire folder of [OMGWTFOTL] to

	ms0:\PSP\GAME\RenPSP\games

(where ms0 is the PlayStation Portable's storage)


◆UNINSTALLATION

Simply delete the entire folder of [OMGWTFOTL] inside

	ms0:\PSP\GAME\RenPSP\games

(Note: This will delete YOUR GAME PROGRESS)


◆STARTUP

Open RenPSP 

◆TECHNICAL SUPPORT

For this PlayStation Port, please neither contact the original
authors, the Haeleth Forums al|together 2006 subforum, nor insani.org.
Instead, contact the following persons stated below:

※lolbot (RenPSP Developer)
・lolbot-at-jabber-dot-ru
・lolbot-underscore-iichan-at-mail-dot-ru

※weetabix (RenPSP VN Port Scriptwriter)
・weetabix-at-jabber-dot-org
・vovo27-underscore-miranemiko-at-yahoo-dot-co-dot-jp


◆SPECIAL THANKS

Special thanks to Naoki Takahashi, the creator of NScripter.  We
would not have even been able to begin making games if your system
did not exist.  Our sincerest gratitude.


◆DISCLAIMERS AND COPYRIGHT INFORMATION

This software comes with no warranty, express or implied.  It is
provided "as is", and the creators of this software shall not be held
liable for any possible damages that might result from the usage of
this software.

It also contains heavy vulgarity and even heavier stupidity.  We
leave it to your judgment as to whether or not to play it.

This software is for personal use only.  You are expressly forbidden
to modify, redistribute, copy, decrypt, or attempt to sell this
software without the express written consent of the authors.

Please contact the original authors should you wish to use pictures
or excerpts or anything else from this game.

DO NOT IMITATE EITHER LOLBOT OR WEETABIX. BE THE GOOD CITIZEN THAT
YOU ARE AND ASK PERMISSION FROM THE AUTHORS. WE KNOW WE ARE IN THE
DEEPEST SHIT POSSIBLE TO GIVE YOU THE VERY BEST, BUT PLEASE, FOR
YOUR OWN GOOD, DO ASK PERMISSION.

MOST IMPORTANTLY, DO NOT REPORT US. WE WILL CHOKE YOUR NECKS IF YOU DO.


◆GENUFLECTWARE

OMGWTFOTL is Genuflectware.

Make sure to turn in the general direction of Sagamihara, Japan, and
genuflect before and after play.


◆TRANSLATOR'S NOTES

Now, finally, the lunatic novel game which received enthusiastic applause
from ... an incredibly small number of rabid Japanese fans ... comes to
invade the English-speaking world!

This game contains strong violence, a multitude of swear words, and
there are many words and concepts that may require a more in-depth
understanding of Japanese culture to fully understand.  The game may
even cause you a gigantic headache due to how stupid it is!

All descriptions of people, behaviors, organizations, cults, and secret
rituals in this game are fiction, and are also random.

There do exist several uniquely Japanese elements in this game, 
including descriptions of certain battleships, professional wrestlers,
cultural icons, and other things.  These are detailed below.

※THE TITLE
・In the original Japanese, this is "Otoko Dogeza Jigoku" -- which translates
  into something like "A Man's Prostration Hell".  Our localization of the
  title into "OMGWTFOTL" essentially has to do with the fact that the
  "dogeza" part of this equation -- which is the most important part -- is
  often symbolically written as orz or OTL.  Take a healthy dose of OMG,
  WTF (which you should be saying QUITE often as you play this), and BBQ, and,
  well, you know.  Finally, there is a recent PSP minigame collection known
  as "BAITO Jigoku", which translates into "Work Hell".  This title was given
  the most stunningly good English localization ever -- "Work Time Fun".  The
  rest, as they say, is history.

※BANCHO
・This is a term used to describe a specific kind of schoolyard bully/
  gang leader; stereotypically, he's seen wearing his uniform in a shady
  manner, with buttons open and such.  There are many examples of Bancho
  in both novel games and anime, including a very funny parody of one in
  one of the characters of "School Rumble".

※THE GREAT KABUKI
・A famous pro wrestler who was the first in the United States to use the
  painted Japanese warrior gimmick.  One of his most famous moves is the
  "mysterious poison mist", in which he sprays, well, a mysterious poison
  mist at his opponent.

※OSAKA BANCHO
・Osaka Bancho, in addition to being a Bancho, speaks in heavy, heavy Osaka-
  ben -- a dialect of Japanese that is stereotypically known for its
  "roughness" and its "earthiness".  We have modeled this in our translation
  by making this character extremely slang-heavy.

※KARL GOTCH
・It is not an exaggeration to say that Karl Gotch was a Japanese Pro
  Wrestling figure equal in stature to, say, Hulk Hogan.  Born in Germany
  in 1924, he had a mediocre career in the United States before moving over
  to Japan, where he found his true calling and revolutionized the state
  of Japanese Pro Wrestling.  Many of the most famous figures in Japanese
  Pro Wrestling have either worked with him or trained under him, and his
  impact on that industry cannot be underestimated.

※KANTO
・The eastern part of Japan.  Includes Kanagawa.  Doesn't include Osaka.

※KANSAI
・The western part of Japan.  Includes Osaka.  Doesn't include Kanagawa.

※YAMATO
・In the context of this game, this is not a reference to the famous heavy
  battleship of the Imperial Japanese Navy of World War II, but to the manga
  that was entitled 沈黙の艦隊, or "The Silent Service".  In it, the Japanese
  Maritime Self-Defense Force jointly develops a nuclear submarine with the US
  Navy; during its maiden voyage, its captain, who is indeed named Kaieda,
  declares the ship an independent country named Yamato.

※CORPSES UNDER THE CHERRY TREE
・There are several sources to this particular legend, which states that cherry
  blossoms could never become so red-pink by themselves -- surely the cherry
  tree must be nourished by human bodies that are buried underneath the tree.
  The storyline of the CLAMP manga "Tokyo Babylon" is one take on this myth.

※TENTACLE BEASTS
・There is a wide sub-genre of 18+ Japanese media dedicated to depictions of
  women as they are raped and violated by nondescript tentacle beasts, often
  demonically summoned, sometimes from outer-space.  This sub-genre has become
  the butt of many, many jokes throughout the years, and the one contained in
  "OMGWTFOTL" is merely the latest in this long string.

※TSUKKOMI AND BOKE
・Two archetypes often used in Japanese stand-up comedy.  The "boke" will either
  say something stupid or wrong, and will be "corrected" by the "tsukkomi" with
  a slap upside the head -- often with a large paper fan known as a "harisen".

I wish to express my gratitude to HANPAMANIA-SOFT for granting me permission
to translate this piece.

I also wish to thank gp32, Haeleth, AstCd2, Spiritsnare, and everyone else
who helped me.  Without you, this translation would have been impossible.

※TRANSLATOR'S BLOG
・http://dekadenbiyori.blog40.fc2.com/

※TRANSLATOR'S EMAIL
・fujiwara102-at-hotmail-dot-com